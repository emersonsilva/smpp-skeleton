var smpp = require('smpp')
var auth = require("./auth")
var sms = require("./sms")
var unidecode = require('unidecode');

const { criaDlrMessage } = require("./auxl")
var {  mo, real_dlr } = require('../controller/queue')

var server = undefined

var TIPOS_DLR = {
  DLR_ENVIADO: "DLR_ENVIADO",
  DLR_VERDADEIRO: "DLR_VERDADEIRO",
}
var SBMT = 0
var DLR = 0
var DLR_RESP = 0
const MEU_DLR = process.env.DLR
console.log(MEU_DLR, MEU_DLR == TIPOS_DLR.DLR_ENVIADO)
var create = () => {
  server = smpp.createServer((session) => {
    // ao receber o bind transceiver
    session.on('bind_transceiver', (pdu) => {
      session.pause()
      //se a sessao ainda nao tiver feito bind
      if (!session.user || !session.password) {
        //verifico o user
        auth.checkUser(pdu, (authorized, my_user) => {
          //se o callback retornar false, entao nao pode logar
          if (!authorized) {
            session.send(pdu.response({ command_status: smpp.ESME_RBINDFAIL }))
            session.close()
            return

          } else {
            //verifico a limitacao de sessoes
            let sessoes = server.sessions.filter((session) => session.user === pdu.system_id)
            if (sessoes.length > my_user.session_limit) {
              session.send(pdu.response({ command_status: smpp.ESME_RBINDFAIL }))
              session.close()
              return
            }
            session.my_user = my_user
            session.user = pdu.system_id
            session.password = pdu.password
            session.ultimoSubmit = undefined
            session.rabbit = undefined
          }
        })
      }

      session.send(pdu.response({ message_id: null }))
      session.resume()
      console.log('logado', session.user, pdu.system_id)

      //metodo mo recebe da fila os MO
      mo('mo_messages', pdu.system_id, (message, connection) => {
        if (session.rabbit_mo === undefined || message == null) {
          session.rabbit_mo = connection
          return
        }
        console.log(pdu.system_id, "=>", message.toString())
        message = unidecode(message)
        let pacoteRabbit = JSON.parse(message)
        let origem = pacoteRabbit.remetente;
        let destino = pacoteRabbit.destino;
        let mensagem = pacoteRabbit.mensagem;
        //limito a 159 caracteres o MO
        if (mensagem.length > 159) mensagem = mensagem.substring(0, 159);

        //disparo o MO
        session.deliver_sm({
          source_addr: origem,
          destination_addr: destino,
          short_message: mensagem,
        });
      })

      //recebe a fila de DLR REAL
      real_dlr('dlr_new_messages', pdu.system_id, (message, connection, ack) => {
        if (session.rabbit_dlr_real === undefined) {
          //recebi a conexao do rabbit
          session.rabbit_dlr_real = connection
          return;
        } else if (message != null) {
          console.log(pdu.system_id, "=>", message.toString())
          let pacoteRabbit = JSON.parse(message)
          DLR_RESP++
          session.deliver_sm(criaDlrMessage(pacoteRabbit.status, pacoteRabbit.id, pacoteRabbit.mensagem, new Date()), () => { }, () => {
            if (ack != undefined) {
              console.log("retornando dlr ack se houver callback")
              ack()
            }
          })
        }

      })
    })

    session.on('error', () => {
      console.log("erro ao retornar envio")
    })

    session.on('submit_sm', (pdu) => {
      let esteSubmit = new Date()
      session.ultimoSubmit = esteSubmit.getTime()
      mensagem = pdu.short_message.message
      msg_payload = (pdu.message_payload == undefined ? "" : pdu.message_payload.message)
      if (mensagem == undefined || mensagem == "") {
        mensagem = msg_payload
      }
      if (mensagem == undefined || mensagem == "") {
        session.send(pdu.response({
          command_status: smpp.ESME_RSUBMITFAIL
        }))
        return;
      }
      console.log(pdu)
      //salvo no redis para outro servico consumir e chamar a api sem degradar
      sms.insertRedis(session.user, session.password, mensagem, pdu.destination_addr, (error, id, message) => {
        if (error) {
          console.log("error 97", error)
          session.send(pdu.response({
            command_status: smpp.ESME_RSUBMITFAIL
          }))
          return
        }

        SBMT++
        session.send(pdu.response({
          message_id: id
        }))

        if (pdu.registered_delivery > 0 && pdu.registered_delivery < 5) {
              sms.gravaPacoteParaDelivery(session.user, id, message, pdu.registered_delivery, esteSubmit, () => {
                if (pdu.registered_delivery === 4) {
                  DLR_RESP++
                  console.log('Message registered delivery:', id, pdu.registered_delivery)
                  session.deliver_sm(criaDlrMessage('ENROUTE', message, esteSubmit))
                }
              })
        }
      })

    })

    session.on('deliver_sm_resp', (pdu) => {
      DLR++;
      total = SBMT + "\t\t" + DLR + "\t\t" + DLR_RESP;
      console.log("SBMT\t\tDLR\t\tDLR_RESP")
      console.log(total)
    })

    session.on('unbind', (pdu) => {
      if (session.rabbit_mo !== undefined) {
        session.rabbit_mo.close((err) => {
          console.log("UNBIND rabbit_mo")
        })
      }

      if (session.rabbit_dlr_real !== undefined) {
        session.rabbit_dlr_real.close((err) => {
          console.log("UNBIND rabbit_mo")
        })
      }

      session.send(pdu.response())
      session.close()
    })

    session.on('enquire_link', (pdu) => {
      console.log("ENQUIRE", session.user)
      session.send(pdu.response())
    })

    session.on('close', () => {
      if (session.rabbit_mo !== undefined) {
        try {
          session.rabbit_mo.close((err) => {
            console.log("CLOSED")
          })
        } catch (e) { }
      }

      if (session.rabbit_dlr_real !== undefined) {
        try {
          session.rabbit_dlr_real.close((err) => {
            console.log("CLOSED")
          })
        } catch (e) { }
      }
    })
  })
}

var listen = () => {
  if (server == undefined) {
    return console.log("FAIL! You need create server")
  }

  server.listen(2775, () => {
    console.log("Gateway SMPP iniciado na porta 2775");
  })
}

module.exports = { create: create, listen: listen }
