const amqp = require('amqplib/callback_api')

const receiver = (exchange, routingKey, callback) => {
  amqp.connect(`amqp://${process.env.RABBIT_SERVER}`, (err, conn) => {
    conn.createChannel((err, channel) => {
      channel.assertExchange(exchange, 'direct', { durable: true })

      channel.assertQueue(`dlr:${routingKey}`, { exclusive: false }, (err, q) => {
        channel.bindQueue(q.queue, exchange, routingKey)
        channel.prefetch(10);
        channel.consume(q.queue, (msg) => {
          callback(msg.content.toString(), null, ()=>{
            channel.ack(msg)
          })
          
          return
        }, { noAck: false })
        

        
      })
    })

    callback(null, conn)
  })
}
const real_dlr = (exchange, routingKey, callback) => {
  amqp.connect(`amqp://${process.env.RABBIT_SERVER}`, (err, conn) => {
    conn.createChannel((err, channel) => {
      channel.assertExchange(exchange, 'direct', { durable: true })

      channel.assertQueue(`real_dlr:${routingKey}`, { exclusive: false }, (err, q) => {
        channel.bindQueue(q.queue, exchange, routingKey)
        channel.prefetch(1);
        channel.consume(q.queue, (msg) => {
          callback(msg.content.toString(), null, ()=>{
            channel.ack(msg)
          })
          
          return
        }, { noAck: false })
        

        
      })
    })

    callback(null, conn)
  })
}


const mo = (exchange, routingKey, callback) => {
  amqp.connect(`amqp://${process.env.RABBIT_SERVER}`, (err, conn) => {
    conn.createChannel((err, channel) => {
      channel.assertExchange(exchange, 'direct', { durable: true })

      channel.assertQueue(`mo:${routingKey}`, { exclusive: false }, (err, q) => {
        channel.bindQueue(q.queue, exchange, routingKey)
        channel.prefetch(1);
        channel.consume(q.queue, (msg) => {
          callback(msg.content.toString(), null)
          channel.ack(msg)
          return
        }, { noAck: false })
      })
    })

    callback(null, conn)
  })
}



module.exports = {
  receiver: receiver,
  mo: mo,
  real_dlr: real_dlr
}
