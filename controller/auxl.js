const formataValorParaDoisAlgarismos = (value) => {
  return value.toString().length === 1 ? `0${value}` : `${value}`
}


const dataFormatada = (data = new Date()) => {
  return new String().concat(
    data.getFullYear().toString().slice(2),
    formataValorParaDoisAlgarismos(data.getMonth() + 1),
    formataValorParaDoisAlgarismos(data.getDate()),
    formataValorParaDoisAlgarismos(data.getHours()),
    formataValorParaDoisAlgarismos(data.getMinutes())
  )
}

const definePropriedades = (status) => {
  switch (status) {
    case 'ENROUTE':
    return {
      esm_class: 4,
      status_cod: 0,
      error: '000'
    }
    break;
    case 'ACCEPTD':
    return {
      esm_class: 4,
      status_cod: 0,
      error: '000'
    }
    break;

    case 'DELIVRD':
      return {
        esm_class: 4,
        status_cod: 2,
        error: '000'
      }
      break;
    
    case 'UNDELIV':
      return {
        esm_class: 4,
        status_cod: 0,
        error: '500'
      }
      break;
    case 'REJECTD':
      return {
        esm_class: 4,
        status_cod: 0,
        error: '1'
      }
      break;
    case 'UNKNOWN':
      return {
        esm_class: 4,
        status_cod: 0,
        error: '550'
      }
      break;
    
    default:
      break;
  }
}

const criaDlrMessage = (status, id, mensagem, submit, callback) => {
  let props = definePropriedades(status)

  try {
    return {
      short_message: `id:${id} sub:001 dlvrd:${props.status_cod} submit date:${dataFormatada(submit)} ` +
      `done date:${dataFormatada()} stat:${status} err:${props.error} text:${mensagem.substring(0, 16)}`, 
      esm_class: props.esm_class
    }

  } catch (e) {
    console.log(e)
    return null
  }
}


module.exports = {
  criaDlrMessage: criaDlrMessage,
}

