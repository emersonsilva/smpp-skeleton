var uuid = require('uuid4')
var normalizeText = require("../utils/normalizeText").normalize
var numeroTelefone = require("../utils/numeroTelefone").trataTelefone
const { Pacote } = require('../models/pacote')
var redis = require("../connect/redis");
var insertToExpire = (id, message)=>{
  try {
    let pacoteRabbit = JSON.parse(message)
    pacoteRabbit['timestamp'] = Math.floor(Date.now() / 1000)
    redis.client.set("EXPIRE:"+id, JSON.stringify(pacoteRabbit), 'EX', 60*60*12)
  } catch(e){
    console.log('erro ao inserir para expirar', id)
  }
}
var insertRedis = (usuario, senha, mensagem, destino, callback) => {
  try {
  let id = uuid()
  let pacote = new Pacote(usuario, senha, normalizeText(mensagem), numeroTelefone(destino))
    redis.client.set(id, pacote.toString(), (error, reply) => {
      if (error) {
        console.log('ERRO AQUI?', error)
        callback(true, error)
        return
      }

      callback(false, id, pacote.toString())
    })
  } catch (e) {
    console.log('OU AQUI?', e)
    callback(true, e)
    return
  }
}

var searchRedisByKey = (id, callback) => {
  redis.client.get(id, (error, reply) => {
    if (error)
      throw error
    
    let pacote = JSON.parse(reply)
    callback(false, { id, pacote })
  })
}

const searchRedisHashByKey = (id, callback) => {
  redis.client.hscan(id, (error, reply) => {
    if (error)
      throw error

    let pacote = JSON.parse(reply)
    callback(false, { id, pacote })
  })
}

const gravaPacoteParaDelivery = (userId, pacote_id, message, tipo, submit, callback) => {
  redis.client.hmset([
    `dlr:${pacote_id}:${userId}`,
    'message', JSON.stringify(message),
    'tipo', tipo,
    'submit', submit], (error, reply) => {

      if (error) {
        throw error
        return
      }
      redis.client.pexpire(`dlr:${pacote_id}:${userId}`, 1000 * 60 * 60 * 24)
      callback()
    })
}

module.exports = {
  insertRedis: insertRedis,
  searchRedisByKey: searchRedisByKey,
  gravaPacoteParaDelivery: gravaPacoteParaDelivery,
  insertToExpire: insertToExpire
}
