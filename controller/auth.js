
var auth = (pdu, callback) => {
  console.log("login in..", pdu)
  system_id = pdu.system_id
  password = pdu.password

  if (system_id && password) {
    var usuarios = require("../login.json")
    for(key in usuarios){
      var user = usuarios[key];
      if(user.system_id == user.system_id && password == user.password){
        return callback(true, user);
      }
    }

    callback(false)
  } else {
    console.log("Sem usuario/senha")
    callback(false)
  }
}

module.exports = { checkUser: auth }
