exports.Pacote = class {
  constructor(login, senha, mensagem, destino) {
    this.login = login
    this.senha = senha
    this.mensagem = mensagem
    this.destino = destino
    Object.freeze(this)
  }

  validate() {
    for (let k of Object.keys(this)) {
      if (this[k] === null || this[k] === undefined) {
        throw new Error(`Propriedade ${k.trim()} não foi definida corretamente.`)
      }
    }
    return true
  }

  toString() {
    if (this.validate())
      return JSON.stringify(this)
  }
}