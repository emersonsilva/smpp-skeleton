#!/usr/bin/env bash

if [ -f docker-compose.yml ]
then
    SERVICO=$(pwd | awk -F'/' '{print $NF}')
    echo "INICIANDO $SERVICO"

    source config.sh
    config_servico

    if [ -f env_var ]
    then
        export $(cat env_var | xargs)
    fi

    docker-compose up -d --build
fi
