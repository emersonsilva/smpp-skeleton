# lists all available targets
list:
	@sh -c "$(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'make\[1\]' | grep -v 'Makefile' | sort"

# required for list
no_targets__:

# test your application (tests in the tests/ directory)

# inicia servico
start:
	@bash start.sh

# parar servico
stop:
	@docker-compose down

# inicia servicos no rancher
rancher:
	@bash rancher.sh