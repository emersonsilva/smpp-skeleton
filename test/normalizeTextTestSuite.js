const describe = require('mocha').describe
const it = require('mocha').it
const expect = require('chai').expect

const normalizaTexto = require('../utils/normalizeText').normalize

describe('Normalizador de Textos', () => {
  describe('Remove acentos de vogais', () => {
    it('Vogais Aa', () => {
      let result = normalizaTexto('áàãâä, ÁÀÃÂÄ')
      expect(result).to.equal('aaaaa, AAAAA')
    })

    it('Vogais Ee', () => {
      let result = normalizaTexto('éèêë, ÉÈÊË')
      expect(result).to.equal('eeee, EEEE')
    })

    it('Vogais Ii', () => {
      let result = normalizaTexto('íìîï, ÍÌÎÏ')
      expect(result).to.equal('iiii, IIII')
    })

    it('Vogais Oo', () => {
      let result = normalizaTexto('óòõôö, ÓÒÕÔÖ')
      expect(result).to.equal('ooooo, OOOOO')
    })

    it('Vogais Uu', () => {
      let result = normalizaTexto('úùûü, ÚÙÛÜ')
      expect(result).to.equal('uuuu, UUUU')
    })
  })

  describe('Remove acento de consoantes', () => {
    it('Consoantes Nn', () => {
      let result = normalizaTexto('ñ, Ñ')
      expect(result).to.equal('n, N')
    })

    it('Consoantes Cc', () => {
      let result = normalizaTexto('ç, Ç')
      expect(result).to.equal('c, C')
    })
  })

  describe('Remove caracteres especiais', () => {
    it('Remove \\', () => {
      let result = normalizaTexto('Busca o dia 11\\12\\2008')
      expect(result).to.equal('Busca o dia 11122008')
    })

    it('Remove qualquer caracter non-ASCII', () => {
      let result = normalizaTexto('¨§ªº°')
      expect(result).to.equal('')
    })
  })
})
