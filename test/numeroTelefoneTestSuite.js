const describe = require('mocha').describe
const it = require('mocha').it
const expect = require('chai').expect

const trataTelefone = require('../utils/numeroTelefone').trataTelefone

describe('Tratador de Numeros de Telefone', () => {
  describe('Trata número com 13 dígitos', () => {
    it('Com prefixo si brasileiro 55ddN########', () => {
      let result = trataTelefone('5531971130196')
      expect(result).to.equal('5531971130196')
    })

    it('Com qualquer outro prefixo si ##ddN########', () => {
      let result = trataTelefone('2331988887777')
      expect(result).to.equal('5531988887777')
    })

    it('Com prefixo si, dd precedido de 0 e sem nono dígito 550dd########', () => {
      let result = trataTelefone('5503171130218')
      expect(result).to.equal('5531971130218')
    })
  })

  describe('Trata número com 12 dígitos', () => {
    it('Com prefixo si brasileiro sem nono digito 55dd########', () => {
      let result = trataTelefone('553171132407')
      expect(result).to.equal('5531971132407')
    })

    it('Com qualquer outro prefixo si sem nono digito ##dd########', () => {
      let result = trataTelefone('223188887777')
      expect(result).to.equal('5531988887777')
    })

    it('Precedido de 0 com nono digito 0ddN########', () => {
      let result = trataTelefone('031971135546')
      expect(result).to.equal('5531971135546')
    })
  })

  describe('Trata número com 11 dígitos', () => {
    it('Com dd e nono dígito dd9########', () => {
      let result = trataTelefone('31971137705')
      expect(result).to.equal('5531971137705')
    })

    it('Precedido de 0 e sem nono dígito 0dd########', () => {
      let result = trataTelefone('03171141047')
      expect(result).to.equal('5531971141047')
    })
  })

  describe('Trata número com 10 dígitos', () => {
    it('Com dd apenas dd########', () => {
      let result = trataTelefone('3171141628')
      expect(result).to.equal('5531971141628')
    })

    it('Com dd inciando em 0 0d########', () => {
      expect(() => trataTelefone('0188887777'))
        .to.throw(Error, "Numero com formato inválido")
    })
  })

  describe('Não permite números com mais de 13 ou menos de 10 dígitos', () => {
    it('Com 14 digitos', () => {
      expect(() => trataTelefone('55031971129178'))
        .to.throw(Error, "Número com formato inválido")
    })

    it('Com 10 digitos', () => {
      expect(() => trataTelefone('888877777'))
        .to.throw(Error, "Número com formato inválido")
    })
  })

  describe('Não permite números com valores alphanuméricos', () => {
    it('Número precedido de sinal de +', () => {
      expect(() => trataTelefone('+553171128909'))
        .to.throw("Número com formato inválido")
    })
  })
})
