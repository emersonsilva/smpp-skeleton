const describe = require('mocha').describe
const it = require('mocha').it
const expect = require('chai').expect

const intervaloDeEnvioPermitido = require('../controller/auxl').intervaloDeEnvioPermitido

describe('Intervalo de Envio Permitido', () => {
  describe('Envio anterior não foi definido', () => {
    it('deve permitir o envio', () => {
      intervaloDeEnvioPermitido(undefined, new Date().getTime(), (result) => {
        expect(result).to.be.true
      })
    })
  })

  describe('Envio anterior definido', () => {
    it('deve permitir envio se delta maior que 50', () => {
      let delta = 51
      intervaloDeEnvioPermitido(new Date().getTime() - delta, new Date().getTime(), (result) => {
        expect(result).to.be.true
      })
    })

    it('não deve permitir envio se delta menor que 50', () => {
      let delta = 49
      intervaloDeEnvioPermitido(new Date().getTime() - delta, new Date().getTime(), (result) => {
        expect(result).to.be.false
      })
    })
  })
})