const redis = require("redis")
const client = redis.createClient(6379, process.env.REDIS_HOST)

client.on('connect', () => {
  console.log('connected')
})

module.exports = { client }
