FROM node:7-slim


RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN npm install -g pm2

COPY package.json /usr/src/app
RUN npm install

COPY . /usr/src/app
EXPOSE 2775

RUN echo America/Sao_Paulo | tee /etc/timezone && \
    dpkg-reconfigure --frontend noninteractive tzdata

CMD ["node", "server.js"]
