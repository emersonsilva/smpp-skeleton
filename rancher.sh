#!/usr/bin/env bash

if [ -f docker-compose.yml ]
then
    PROJETO=$(pwd | awk -F'/' '{print $NF}')
    echo "INICIANDO $PROJETO"

    source config.sh
    config_rancher
    config_servico

    if [ -f env_var ]
    then
        export $(cat env_var | xargs)
    fi

    RANCHER_COMPOSE=./rancher-compose-$RANCHER_VERSION/rancher-compose

    $RANCHER_COMPOSE --url "http://$RANCHER_HOST/" --project-name $PROJETO rm --force smpp-gateway
    $RANCHER_COMPOSE --url "http://$RANCHER_HOST/" --project-name $PROJETO up -d -c --force-recreate smpp-gateway
fi