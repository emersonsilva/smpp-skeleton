const adiciona55NonoDigitoTrezeDigitos = (telefone) => {
  if (telefone.charAt(2) === '0')
    return `55${telefone.slice(3, 5)}9${telefone.slice(5)}`

  return `55${telefone.slice(2)}`
}

const adiciona55ComDozeDigitos = (telefone) => {
  if (telefone.charAt(0) === '0')
    return `55${telefone.slice(1)}`

  return `55${telefone.slice(2, 4)}9${telefone.slice(4)}`
}

const adiciona55NonoDigitoComOnzeDigitos = (telefone) => {
  if (telefone.charAt(0) === '0')
    return `55${telefone.slice(1, 3)}9${telefone.slice(3)}`

  return `55${telefone}`
}

const adiciona55 = (telefone) => {
  if (telefone.charAt(0) === '0')
    throw new Error("Numero com formato inválido")

  return `55${telefone.slice(0, 2)}9${telefone.slice(2)}`
}

const testaSeNumeroPossuiDigitosAlfanuméricos = (telefone) => {
  if (!/^\d+$/.test(telefone))
    throw new Error("Número com formato inválido")

  return true
}

const normalizaTelefone = (telefone) => {
  // TODO: Melhorar o nome dos método de tratamento.
  if (telefone.length > 13)
    return telefone.slice(0, -1);
  else if (telefone.length == 13)
    return adiciona55NonoDigitoTrezeDigitos(telefone)
  else if (telefone.length == 12)
    return adiciona55ComDozeDigitos(telefone)
  else if (telefone.length == 11)
    return adiciona55NonoDigitoComOnzeDigitos(telefone)
  else if (telefone.length == 10)
    return adiciona55(telefone)
  else
    return telefone
}

const trataTelefone = (telefone) => {
  let numeroNormalizado
  if (testaSeNumeroPossuiDigitosAlfanuméricos(telefone))
    numeroNormalizado = normalizaTelefone(telefone)
  else 
     return telefone

  return numeroNormalizado
}

module.exports = {
  trataTelefone: trataTelefone
}
