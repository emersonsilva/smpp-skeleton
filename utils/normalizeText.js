const normalize = (str) => {
  if (str === undefined || str === '' || str === null)
    throw new Error("Mensagem inválida.")

  return String(str)
    .replace(/[áàãâä]/g, 'a')
    .replace(/[ÁÀÃÂÄ]/g, 'A')
    .replace(/[éèêë]/g, 'e')
    .replace(/[ÉÈÊË]/g, 'E')
    .replace(/[íìîï]/g, 'i')
    .replace(/[ÍÌÎÏ]/g, 'I')
    .replace(/[óòõôö]/g, 'o')
    .replace(/[ÓÒÕÔÖ]/g, 'O')
    .replace(/[úùûü]/g, 'u')
    .replace(/[ÚÙÛÜ]/g, 'U')
    .replace(/ñ/g, 'n')
    .replace(/Ñ/g, 'N')
    .replace(/[ç]/g, 'c')
    .replace(/[Ç]/g, 'C')
    .replace(/[\x5C]/g, "")
    .replace(/[\x7B-\x7D]/g, "")
    .replace(/[^\x00-\x7F]/g, "")
}

module.exports = { normalize }
